const path = require('path')
const webpack = require('webpack')

const config = {
    entry: './src/index.js',
    module: {
        rules: [
            {
                test: /\.?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    externals: {
        'chart.js': 'Chart'
    },
    plugins: [
        new webpack.ProvidePlugin({
            Chart: 'chart.js'
        })
    ]
}

const dist = Object.assign({}, config, {
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'chartjs-plugin-axislegend.js'
    }
})

const prod = Object.assign({}, config, {
    mode: 'production',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'chartjs-plugin-axislegend.min.js'
    }
})

module.exports = [
    dist, prod
]