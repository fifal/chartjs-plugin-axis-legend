## chartjs-plugin-axis-legend
[![npm version](https://badge.fury.io/js/chartjs-plugin-axis-legend.svg)](https://badge.fury.io/js/chartjs-plugin-axis-legend)

This plugin adds small squares above the Y axis to see which dataset corresponds to which axis. 
> NOTE: This plugin is in development and the legend may not render properly for some values

![example image](doc/example.png)

### Installation
You can install the plugin using the npm:
```
npm install chartjs-plugin-axis-legend
```

### Usage
Plugin has the following settings:
- `squareSize`: size of the square in pixels
- `squareMargin`: margin among squares
- `margin`: margin from the left side of the axis

Example code on how to use this plugin:
```javascript
import Chart from 'chart.js'
import AxisLegendPlugin from 'chartjs-plugin-axis-legend'

// Register plugin
Chart.plugins.register(AxisLegendPlugin)

// Plugin options
const options = {
    plugins: {
        axislegend: {
            squareSize: 8,
            squareMargin: 2,
            margin: 3
        }
    }
}

// Create Chart as usual
... 
```