'use strict'

const pluginName = 'axislegend'
const minPaddingTop = 24

/**
* Calculates top padding for the legend squares
* 
* @param {Chart} chartInstance
*/
function calculatePadding(chartInstance) {
    const title = chartInstance.options.title
    const titlePadding = title.display ? (title.fontSize) : 0
    const squareWidth = chartInstance.options.plugins.axislegend.squareSize
    const squareMargin = chartInstance.options.plugins.axislegend.squareMargin

    let axesCount = []
    chartInstance.config.data.datasets.forEach((dataset) => {
        if (!(dataset.yAxisID in axesCount)) {
            axesCount[dataset.yAxisID] = 1
        } else {
            axesCount[dataset.yAxisID]++
        }
    })

    // Counts maximum squares per axis for the horizontal direction, at this point we do not have 
    // access to the scale.width as it is not initialized yet. This calculates worst case scenario – very small axis width
    const horizontalSquaresPerAxis = Math.floor(22 / (squareWidth + squareMargin))
    let verticalSquaresPerAxis = Math.max(...Object.values(axesCount)) / horizontalSquaresPerAxis
    if (verticalSquaresPerAxis < 1) {
        verticalSquaresPerAxis = 1
    }

    const totalHeight = verticalSquaresPerAxis * (squareWidth + squareMargin)

    if (titlePadding > totalHeight) {
        console.log("here")
        return 0
    }

    const result = totalHeight - titlePadding

    // When font size is greater then total height of our squares
    if (result <= 0) {
        return totalHeight
    }

    // If padding from the top is small than minimum padding, apply minimum padding
    if (result < minPaddingTop) {
        return minPaddingTop + (squareWidth * verticalSquaresPerAxis)
    }

    return result
}

/**
 * Checks if configuration contains all required fields
 * @param {Chart} chartInstance 
 */
function checkPluginConfiguration(chartInstance) {
    const requiredFields = { 'squareSize': 8, 'squareMargin': 2, 'margin': 4 }

    if (!(pluginName in chartInstance.options.plugins)) {
        console.warn("Options are missing configuration for the 'axislegend' plugin.")
        chartInstance.options.plugins[pluginName] = {}
    }

    Object.keys(requiredFields).forEach((field) => {
        if (!(field in chartInstance.options.plugins[pluginName])) {
            console.warn("Plugin 'axislegend' configuration is missing '" + field + "', using default value.")
            chartInstance.options.plugins[pluginName][field] = requiredFields[field]
        }
    })
}


const AxisLegendPlugin = {
    id: pluginName,


    beforeInit: (chartInstance) => {
        checkPluginConfiguration(chartInstance)

        const squareSize = chartInstance.options.plugins.axislegend.squareSize

        if (squareSize < 4 || squareSize > 16) {
            console.warn("Square size is too small, or too big. Allowed range is (4-16). Using default value.")
            chartInstance.options.plugins.axislegend.squareSize = 8
        }

        const legend = chartInstance.options.legend
        if (legend.display && legend.position === "top") {
            console.warn("When using the axis legend plugin, the legend must not be at the top, or the legend must be disabled. Moving legend to the bottom.")
            chartInstance.options.legend.position = "bottom"
        }
    },

    beforeLayout: (chartInstance) => {
        chartInstance.options.layout.padding.top = calculatePadding(chartInstance) + chartInstance.options.plugins.axislegend.paddingTop ?? 0
    },

    afterDraw: (chartInstance) => {
        // Find first scale that is on the right side or on the left side, so we can calculate padding from the top
        const scaleTop = Object.values(chartInstance.scales).find((scale) => scale.options.position === "left" || scale.options.position === "right")
        const paddingTop = scaleTop.top - minPaddingTop

        const squareSize = chartInstance.options.plugins.axislegend.squareSize
        const margin = chartInstance.options.plugins.axislegend.margin
        const squareMargin = chartInstance.options.plugins.axislegend.squareMargin

        let drawn = []
        chartInstance.config.data.datasets.forEach((dataset) => {
            let axisId = dataset.yAxisID
            const axisWidth = chartInstance.scales[axisId].width - margin

            if (!(axisId in drawn)) {
                drawn[axisId] = { "count": 0, "row": 0 }
            } else {
                drawn[axisId].count++
            }

            let position = chartInstance.scales[axisId].left + margin + ((squareSize + squareMargin) * drawn[axisId].count)
            let minPosition = chartInstance.scales[axisId].left

            if ((margin + position - minPosition) >= Math.floor(axisWidth)) {
                drawn[axisId].count = 0
                drawn[axisId].row++
                position = chartInstance.scales[axisId].left + margin + ((squareSize + squareMargin) * drawn[axisId].count)
            }

            chartInstance.ctx.fillStyle = dataset.backgroundColor
            chartInstance.ctx.fillRect(position, paddingTop - ((squareSize + squareMargin) * drawn[axisId].row), squareSize, squareSize)
        })
    }
}

export default AxisLegendPlugin